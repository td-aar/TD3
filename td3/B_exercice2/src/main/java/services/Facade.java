package services;

import dtos.UserDto;
import entities.User;
import exceptions.UserAllreadyExistsException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class Facade {
    // Injection de l'entity manager, pour accès à la BD
    @PersistenceContext
    private EntityManager em;

    // On n'utilise plus de map : on a une base de données
    //private Map<String,String> users;

    // le peuplement se fait maintenant avec un script sql
   /* @PostConstruct
    public void fillMap(){
        users=new HashMap<>();
        users.put("alice","alice");
        users.put("bob","bob");
    }
    */


    public boolean checkLP(String login,String password) {
        // On va maintenant chercher l'utilisateur dans la BD à partir du login
        User user=em.find(User.class,login);
        if (user==null) {
            return false;
        } else {
            return (user.getPassword().equals(password));
        }
   }

   @Transactional
   public User createUser(String login,String password) throws UserAllreadyExistsException {
        User user=em.find(User.class,login);
        if (user!=null) {
            throw new UserAllreadyExistsException();
        }
        user =new User(login,password);
        em.persist(user);
        return user;
   }

    public List<UserDto> getUsers() {
        Query q = em.createQuery("select u from User u");
        List<User> users = q.getResultList();
        return users.stream().map(User::toDto).collect(Collectors.toList());
    }

    @Transactional
    public void removeUser(String login) throws Exception {
        User user = em.find(User.class, login);
        if(user == null) {
            throw new Exception("L'utilisateur n'existe pas : " + login);
        }
        em.remove(user);
    }

}
