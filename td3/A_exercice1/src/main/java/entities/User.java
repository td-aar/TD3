package entities;

import dtos.UserDto;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {
    @Id
    private String login;
    private String password;

    public User(){}

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    /*
    On n'autorise pas la modif de login, qui est la clé...
    public void setLogin(String login) {
        this.login = login;
    }
     */

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static UserDto toDto(User u) {
        UserDto userDto = new UserDto();
        userDto.setLogin(u.getLogin());
        userDto.setPassword(u.getPassword());
        return userDto;
    }
}
